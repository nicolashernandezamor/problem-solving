#!/bin/python3
import math
import os
import random
import re
import sys
from itertools import permutations
from array import array

import numpy


def print_matrix(matrix):
    print('--------------------------')
    m = list(matrix).copy()
    m.insert(0, list(i for i in range(len(m))))
    m.insert(1, list('-' * len(m)))
    for i in m:
        print('\t'.join(map(str, i)))
    print('--------------------------')


def rotate_matrix(matrix):
    return list(zip(*matrix))[::-1]


def rotate_and_permutate(matrix):
    all_permutations = []
    rotated = matrix
    for i in range(4):
        all_permutations.extend(list(map(lambda x: list(x), permutations(rotated))))
        rotated = rotate_matrix(rotated)

    return all_permutations


def is_magic_square(matrix):
    n = len(matrix)
    magic_number = (n ** 2 + 1) * n // 2
    return (
            set(range(1, n ** 2 + 1)) == set(i for r in matrix for i in r) and
            all(sum(r) == magic_number for r in (*matrix, *zip(*matrix))) and
            sum(matrix[i][i] for i in range(n)) == sum(matrix[i][-i - 1] for i in range(n)) == magic_number
    )


def discrete_position(i, j, n):
    _i = i
    _j = j

    if _i <= -1:
        _i = n + _i

    if _i >= n:
        _i = _i - n

    if _j <= -1:
        _j = n + _j

    if _j >= n:
        _j = _j - n

    return [_i, _j]


def occupied_position(i, j, matrix):
    return matrix[i] and matrix[i][j] is not None;


def check_conditions(i, j, n, matrix):
    outbound = i == -1 and j == n
    discrete = 0 <= i < n and 0 <= j < n
    occupied = discrete and occupied_position(i, j, matrix)
    return [outbound, discrete, occupied]


def next_position(i, j, n, matrix):
    _i = i - 1
    _j = j + 1
    found_position = False
    # print(f'Incoming ------------ {i} {j}')
    iterator = 0
    while not found_position:
        iterator = iterator + 1
        outbound, discrete, occupied = check_conditions(_i, _j, n, matrix)
        # print(f'({_i}, {_j})')
        # print(f'Discrete: {discrete}, Occupied: {occupied}, Outbound: {outbound}')

        if outbound:
            _i = 0
            _j = n - 2
            continue

        if not discrete:
            _i, _j = discrete_position(_i, _j, n)
            continue

        if occupied:
            _i = _i + 1
            _j = _j - 2
            continue

        # print(f'Found Position: {discrete and not occupied}')
        found_position = discrete and not occupied

    # print(f'Returning ({_i, _j})')
    return [_i, _j]


def magic_nxn_square_permutations(n):
    starting_point = [math.floor(n / 2), n - 1]
    matrix = [[None for i in range(n)] for i in range(n)]
    values = list(range(n ** 2 + 1))[2:n ** 2 + 1]
    last = [starting_point[0], starting_point[1]]
    matrix[last[0]][last[1]] = 1

    for number in values:
        last = next_position(last[0], last[1], n, matrix)
        matrix[last[0]][last[1]] = number
    return filter(lambda x: is_magic_square(x), remove_duplicates(rotate_and_permutate(matrix)))


def remove_duplicates(matrix_list):
    # TODO : Develop a way to remove duplicates.
    return matrix_list


def evaluate_mutation_costs(matrix, magic_squares):
    totals = []
    # Run through all possible Magic Squares
    for magic_square in magic_squares:
        total = 0
        for magic_square_row, matrix_row in zip(magic_square, matrix):
            for magic_square_element, input_matrix_element in zip(magic_square_row, matrix_row):
                # Run through all matrix(i,j) comparing with magic_square(i,j)
                # If elements are different, calculate the cost of mutating it to match it and add it to total
                if not magic_square_element == input_matrix_element:
                    total += abs(magic_square_element - input_matrix_element)
        # Save the total cost of this mutation
        totals.append(total)
    return totals


# Complete the formingMagicSquare function below.
def forming_magic_square(s):
    return min(evaluate_mutation_costs(s, magic_nxn_square_permutations(3)))


if __name__ == '__main__':
    s = []

    for _ in range(3):
        s.append(list(map(int, input().rstrip().split())))
    forming_magic_square()
    result = 'resulted'
    result = forming_magic_square(s)
    print(str(result) + '\n')
