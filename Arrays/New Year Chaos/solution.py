#!/bin/python3

import math
import os
import random
import re
import sys


#
# Complete the 'minimumBribes' function below.
#
# The function accepts INTEGER_ARRAY q as parameter.
#

def minimumBribes(q):
    # Each number N should be on the position N-1.
    # Whatever movement someone has made is made through bribing someone in front.
    # From so we can deduce,
    #   For any P in Q,
    #   The number of elements greater than P in front of the row has bribed P
    #   Bribe a number means a move, so sum of the actions is the result.
    #
    # On the other hand, if any given number is far than 3 places ahead of its original      # position, the row has become too chaotic.
    Q = [P - 1 for P in q]

    too_chaotic = False
    bribes = 0

    for i, P in enumerate(Q):

        if P - i > 2:
            too_chaotic = True
            break

        for j in range(max(P - 1, 0), i):
            if Q[j] > P:
                bribes += 1

    response = "Too chaotic" if too_chaotic else bribes
    print(response)


if __name__ == '__main__':
    t = int(input().strip())

    for t_itr in range(t):
        n = int(input().strip())

        q = list(map(int, input().rstrip().split()))

        minimumBribes(q)
