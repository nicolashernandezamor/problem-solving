#!/bin/python3

import math
import os
import random
import re
import sys


# Complete the minimumSwaps function below.
def minimumSwaps(arr):
    swaps = 0
    positionsMap = {}
    for i, d in enumerate(arr):
        positionsMap[d] = i

    for key, value in enumerate(positionsMap):
        current = key + 1
        position = positionsMap[current]
        onPosition = arr[key]

        if not onPosition == current:
            arr[key], arr[position] = arr[position], arr[key]
            positionsMap[current] = key
            positionsMap[onPosition] = position
            swaps += 1

    return swaps


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    res = minimumSwaps(arr)

    fptr.write(str(res) + '\n')

    fptr.close()