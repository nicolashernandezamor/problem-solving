#!/bin/python3

import math
import os
import random
import re
import sys


def hourGlassValues(coordinates, arr):
    hourglassSum = 0
    for coordinate in coordinates:
        hourglassSum += arr[coordinate[0]][coordinate[1]]
    return hourglassSum


def hourGlassCoordinates(i, j):
    return [(i, j), (i, j + 1), (i, j + 2), (i + 1, j + 1), (i + 2, j), (i + 2, j + 1), (i + 2, j + 2)]


# Complete the hourglassSum function below.
def hourglassSum(arr):
    # For the given pattern
    # The starting point limits are always i = n - 2 (-1), j = n - 2 (-1)
    c = len(arr) - 2
    startingPoints = [(i, j) for i in range(c) for j in range(c)]
    # O(c^2)
    coordinates = list(map(lambda x: hourGlassCoordinates(x[0], x[1]), startingPoints))
    # O(c^2)
    values = list(map(lambda x: hourGlassValues(x, arr), coordinates))
    # O(c^4)
    return max(values)


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    arr = []

    for _ in range(6):
        arr.append(list(map(int, input().rstrip().split())))

    result = hourglassSum(arr)
    print(result)
    # fptr.write(str(result) + '\n')
    #
    # fptr.close()
